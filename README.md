//------------------ Install and setup
    ng add @nguniversal/express-engine --clientProject <projectName>
    
    //if u have firebase
    npm i @angular/fire firebase 
    npm i ws xhr2 -D
    (global as any).WebSocket = require('ws') //server.ts first line
    (global as any).XMLHttpRequest = require('xhr2') //server.ts second line
    
    //then build and serve
    npm run build:ssr
    npm run serve:ssr
    
    //------------------ Create meta tags
    
    import { Component, OnInit } from '@angular/core';
    import { Title, Meta } from '@angular/platform-browser'
    
    @Component({
      selector: 'app-about',
      templateUrl: './about.component.html',
      styleUrls: ['./about.component.scss']
    })
    export class AboutComponent implements OnInit {
      data = {
        name: 'Michael Jordan',
        bio: 'Former baseball player',
        image: 'https://goo.gl/hfvwfq'
      }
      constructor(private title: Title, private meta: Meta) { }
    
      ngOnInit() {
        this.title.setTitle(this.data.name);
        this.meta.addTags([
          { name: 'twitter:card', content: 'summary' },
          { name: 'og:url',content:'/about'},
          {name: 'og:title',content:this.data.name},
          {name: 'og:description',content:this.data.bio},
          {name: 'og:image',content:this.data.image}
        ])
      }
    
    }
    
    //---- deploy to AppEngine have to installed gcloud (cloud sdk)
    create app.yaml file in app folder:
    	runtime: nodejs8
    in package.json:
    	"start":"npm run serve:ssr" //its sould point to the
    
    npm run build:ssr
    gcloud app deploy // command and its done!
    
    //------------ deploy to Firebase Cloud function
    firebase init
    	//hosting and function(Typescript) select
    	//point to the dist/browser
    	//Yes to single page app
    	//No to rewrite
    in firebase.json:
    	"rewrites": [
    	      {
    	        "source": "/**",//iportant for routing
    	        "function": "ssr"
    	      }
    	    ]
    
    cd functions
    npm i fs-extra
    npm i typescript@latest --save-dev
    touch cp-angular.js
    
    in cp-angular.js:
    		const fs = require('fs-extra');
    		
    		(async () => {
    		
    		    const src = '../dist';
    		    const copy = './dist';
    		
    		    await fs.remove(copy);
    		    await fs.copy(src, copy);
    		})();
    in package.json: //inside functions	folder
    	"name": "functions",
    	  "engines": {
    	    "node": "8"
    	  },
    		"scripts": {
        "build": "node cp-angular && tsc",
    		 ...
      },
    
    in index.ts //inside functions folder
    	import * as functions from 'firebase-functions';
    	const universal = require(`${process.cwd()}/dist/server`).app;
    	
    	export const ssr = functions.https.onRequest(universal);
    
    open original server.ts // in root folder
    	"add export"const app.. => export const app..
    	"comment out the last app.listen(PORT ..."
    
    in webpack.server.config.js:
    	externals:[
        /^firebase/
      ],
    	output: {
        // Puts the output at the root of the dist folder
        path: path.join(__dirname, 'dist'),
        library:'app',//important
        libraryTarget:'umd',//important
        filename: '[name].js'
      },
    in root:
    npm run build:ssr
    cd functions
    npm run build
    firebase serve
    firebase deploy